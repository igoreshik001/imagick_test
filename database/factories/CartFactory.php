<?php

namespace Database\Factories;

use App\Models\Cart;
use Illuminate\Database\Eloquent\Factories\Factory;

class CartFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Cart::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $this->faker->randomDigitNotNull,
        'user' => $this->faker->text,
        'product_id' => $this->faker->randomDigitNotNull,
        'product' => $this->faker->text,
        'price' => $this->faker->word,
        'quantity' => $this->faker->randomDigitNotNull,
        'url' => $this->faker->text,
        'picture_web' => $this->faker->word,
        'picture_src' => $this->faker->word,
        'picture' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
