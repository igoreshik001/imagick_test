@extends('layouts.test')

@section('select')
	<label for="entity" class="form-label">Entity</label>
	<select id="entity" class="form-select">
		@foreach($entities as $key => $entity)
			<option value="{{$key}}" {{$key == $entity_select ? 'selected' : ''}}>{{$entity}}</option>
		@endforeach
	</select>

	<label for="rama" class="form-label">Rama</label>
	<select id="rama" class="form-select">
		@foreach($ramas as $key => $rama)
			<option value="{{$key}}">{{$rama}}</option>
		@endforeach
	</select>

	<label for="kant" class="form-label">Kant</label>
	<select id="kant" class="form-select">
		<option value="0">none</option>
		@foreach($kants as $key => $kant)
			<option value="{{$key}}">{{$kant}}</option>
		@endforeach
	</select>

	@if ($vstavki)
		<label for="vstavka" class="form-label">Vstavka</label>
		<select id="vstavka" class="form-select">
			<option value="0">none</option>
			@foreach($vstavki as $key => $vstavka)
				<option value="{{$key}}">{{$vstavka}}</option>
			@endforeach
		</select>
	@endif
@endsection

@section('image')
	<div class="d-flex">
		<img class="mx-auto my-5" id='image' src="/get_image?entity=0&rama=2&kant=0&vstavka=0">
	</div>

	<script>
		$(document).ready(function() {
			$("img").click(function() {
              // Change src attribute of image
              var entity = $('#entity').find(":selected").val();
              var kant = $('#kant').find(":selected").val();
              var rama = $('#rama').find(":selected").val();
              var vstavka = $('#vstavka').find(":selected").val();
              $("#image").attr("src", "/get_image?entity=" + entity + "&rama=" + rama + "&kant=" + kant + "&vstavka=" + vstavka);
	        });

	        $("#rama").on('change', function() {
              // Change src attribute of image
              var entity = $('#entity').find(":selected").val();
              var kant = $('#kant').find(":selected").val();
              var rama = $('#rama').find(":selected").val();
              var vstavka = $('#vstavka').find(":selected").val();
              $("#image").attr("src", "/get_image?entity=" + entity + "&rama=" + rama + "&kant=" + kant + "&vstavka=" + vstavka);
	        });

	        $("#kant").on('change', function() {
              // Change src attribute of image
              var entity = $('#entity').find(":selected").val();
              var kant = $('#kant').find(":selected").val();
              var rama = $('#rama').find(":selected").val();
              var vstavka = $('#vstavka').find(":selected").val();
              $("#image").attr("src", "/get_image?entity=" + entity + "&rama=" + rama + "&kant=" + kant + "&vstavka=" + vstavka);
	        });

	        $("#vstavka").on('change', function() {
              // Change src attribute of image
              var entity = $('#entity').find(":selected").val();
              var kant = $('#kant').find(":selected").val();
              var rama = $('#rama').find(":selected").val();
              var vstavka = $('#vstavka').find(":selected").val();
              $("#image").attr("src", "/get_image?entity=" + entity + "&rama=" + rama + "&kant=" + kant + "&vstavka=" + vstavka);
	        });

			$('#entity').on('change', function () {
				location.href = '/img/' + $('#entity').find(":selected").val();
			});
		});
	</script>
@endsection