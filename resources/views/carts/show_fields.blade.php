<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $cart->user_id }}</p>
</div>

<!-- User Field -->
<div class="col-sm-12">
    {!! Form::label('user', 'User:') !!}
    <p>{{ $cart->user }}</p>
</div>

<!-- Product Id Field -->
<div class="col-sm-12">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{{ $cart->product_id }}</p>
</div>

<!-- Product Field -->
<div class="col-sm-12">
    {!! Form::label('product', 'Product:') !!}
    <p>{{ $cart->product }}</p>
</div>

<!-- Price Field -->
<div class="col-sm-12">
    {!! Form::label('price', 'Price:') !!}
    <p>{{ $cart->price }}</p>
</div>

<!-- Quantity Field -->
<div class="col-sm-12">
    {!! Form::label('quantity', 'Quantity:') !!}
    <p>{{ $cart->quantity }}</p>
</div>

<!-- Url Field -->
<div class="col-sm-12">
    {!! Form::label('url', 'Url:') !!}
    <p>{{ $cart->url }}</p>
</div>

<!-- Picture Web Field -->
<div class="col-sm-12">
    {!! Form::label('picture_web', 'Picture Web:') !!}
    <p>{{ $cart->picture_web }}</p>
</div>

<!-- Picture Src Field -->
<div class="col-sm-12">
    {!! Form::label('picture_src', 'Picture Src:') !!}
    <p>{{ $cart->picture_src }}</p>
</div>

<!-- Picture Field -->
<div class="col-sm-12">
    {!! Form::label('picture', 'Picture:') !!}
    <p>{{ $cart->picture }}</p>
</div>

