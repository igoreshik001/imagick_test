<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Imagick;
use Illuminate\Support\Facades\DB;

class ImagickController extends Controller
{
    public function index(Request $request, $entity_select = 0)
    {

        // $image = new Imagick();
        // $image->readImage('C:\xampp\htdocs\insta\rama.png');

        // $image2 = new Imagick();
        // $image2->readImage('C:\xampp\htdocs\insta\vstavka.png');

        // $image3 = new Imagick();
        // $image3->readImage('C:\xampp\htdocs\insta\ruchka.png');
        // // $image2->shadeImage(false, 45, 20);

        // $image->compositeImage($image2, Imagick::COMPOSITE_OVER, 0, 0);
        // $image->compositeImage($image3, Imagick::COMPOSITE_OVER, 0, 0);

        // // сохранить изображение
        // // $image->writeImage('result_image.png');

        // // или отобразить изображение
        // header('Content-Type: image/' . $image->getImageFormat());
        // echo $image;
        $entities = ['1e', '2e', '3e', '4e', '5e', '6e', '7e', '9e', '10e', '11e', '12e'];
        $vstavki = false;


        $ramas = scandir(public_path().'\E\rama');
        unset($ramas[0]);
        unset($ramas[1]);

        if (file_exists(public_path().'\E\kant\\'.$entities[$entity_select])) {
            $kants = scandir(public_path().'\E\kant\\'.$entities[$entity_select]);
            unset($kants[0]);
            unset($kants[1]);
        } else {
            $kants = scandir(public_path().'\E\kant\1e');
            unset($kants[0]);
            unset($kants[1]);
        }

        if (file_exists(public_path().'\E\vstavka\\'.$entities[$entity_select])) {
            $vstavki = scandir(public_path().'\E\vstavka\\'.$entities[$entity_select]);
            unset($vstavki[0]);
            unset($vstavki[1]);
        }
        // dd($kants);


        // $entities = DB::table('eav_entity')->where('super_entity_id', 17)->orderBy('id')->get();
        // dd($entities);

        return view('pages.test', ['entity_select' => $entity_select, 'entities' => $entities, 'ramas' => $ramas, 'kants' => $kants, 'vstavki' => $vstavki]);

    }
}