<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Models\Images;
use Image;
use Imagick;

class GetImageController extends Controller
{

    public function index(Request $request)
    {
      // dd($request->kant);
      $image = $this->addLayer($request);
      $image->scaleImage( 161, 0);
      header('Content-Type: image/' . $image->getImageFormat());

      echo $image;
   }

   public function addLayer($request)
   {

      $entities = ['1e', '2e', '3e', '4e', '5e', '6e', '7e', '9e', '10e', '11e', '12e'];

      $files = scandir(public_path().'\E\rama');
      $image = new Imagick();
      $image->readImage(public_path().'\E\rama\\'.$files[$request->rama]);
      $kant_ent = '1e';

      if (file_exists(public_path().'\E\kant\\'.$entities[$request->entity])) {
         $kant_ent = $entities[$request->entity];
      }

      if ($request->kant > 0) {
         $files = scandir(public_path().'\E\kant\\'.$kant_ent);
         $kant = new Imagick();
         $kant->readImage(public_path().'\E\kant\\'.$kant_ent.'\\'.$files[$request->kant]);
         $image->compositeImage($kant, Imagick::COMPOSITE_OVER, 0, 0);
      }

      if ($request->vstavka > 0 && file_exists(public_path().'\E\vstavka\\'.$entities[$request->entity])) {
         $files = scandir(public_path().'\E\vstavka\\'.$entities[$request->entity]);
         $vstavka = new Imagick();
         $vstavka->readImage(public_path().'\E\vstavka\\'.$entities[$request->entity].'\\'.$files[$request->vstavka]);
         $image->compositeImage($vstavka, Imagick::COMPOSITE_OVER, 0, 0);
      }

      $files = scandir(public_path().'\E\ruchka');
      $ruchka = new Imagick();
      $ruchka->readImage(public_path().'\E\ruchka\\'.$files[2]);
      $image->compositeImage($ruchka, Imagick::COMPOSITE_OVER, 0, 0);
      return $image;
   }
}