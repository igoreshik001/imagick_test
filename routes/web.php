<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/', [Controllers\EavPictureNewController::class,'index'])->name('index');
// Route::get('lk41', [Controllers\Lk41Controller::class,'index'])->name('lk41');
// Route::get('lk42', [Controllers\Lk42Controller::class,'index'])->name('lk42');
// Route::get('ag6', [Controllers\Ag6Controller::class,'index'])->name('ag6');
Route::get('img/{entity?}', [Controllers\ImagickController::class,'index'])->name('img');
Route::get('/', [Controllers\ImagickController::class,'index'])->name('home');
Route::get('test', [Packages\My\TestController::class,'index'])->name('test');
Route::get('get_image', [Controllers\GetImageController::class,'index'])->name('getimage');






// Route::resource('carts', App\Http\Controllers\CartController::class);
